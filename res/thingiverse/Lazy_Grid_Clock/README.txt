                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:3241802
Lazy Grid Clock by parallyze is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

So here it is, finally. My first project using a single led strip only... xD

Replacement/Alternative for: https://www.thingiverse.com/thing:2930831

<b>Update:</b>
Added a Front Frame which can be mounted to hide the screws on the front (and change appearance/color of the whole thing). Have a look at the instructions further down below.

-- 

A few months ago I uploaded a LED grid. But it seems like people aren't using it for own stuff most of the time - instead they use it as a clock using the supplied example sketch.
That's totally fine - it's just not, what I had in mind when building it. :D
24h format is only supported if built with an extension and most of the time about 50% - 65% of the leds don't do anything.

To fix these "problems" I created this Lazy Grid Clock-Thing.

Features:

1. Single continuous led strip (83 leds) to create a 7x11 pixel matrix
2. Fewer parts than before
3. Even less solder work
4. Customizable to a certain degree (front bezel height/color)
5. Still usable for own projects (lots of screw holes to connect it to other things)
6. If used as a clock only, there's much less LEDs wasted than using the old grid + extension


To build the grid/clock as shown you will need:

1x LGC_Frame_v1.STL
1x LGC_Frame_Back_v1.STL
1x LGC_Front_Bezel_Thin_v1.STL
1x LGC_Elec_Case_v1.STL (including feet)

1x Arduino (Nano or Pro Mini, case fits both)
83x WS2812 LEDs (60 leds/m)
1x resistor (300-500 ohms, I'm using 330)
1x RTC module (DS3231)
2x push buttons (6mm x 6mm)

1 sheet of paper (120mm x 185mm, thin paper)

19x M3x6 screws

Other requirements:
- Working Arduino IDE including the used libraries
- Basic knowledge of what you're doing.

This thing is meant for people who played around a bit with a few WS2812 leds to gather some experience and who are now looking for an easy way to get a working grid. If you're totally new to this you might want to take a look at Adafruits Uberguide or other similar stuff (FastLED user groups).

I will only show you how the grid is put together here - electronics are the same as in many of my other things.

Buttons, RTC and Arduino/Power are connected the same, so you can use my instructions here: https://www.thingiverse.com/thing:3014572 and here (video included): https://www.thingiverse.com/thing:3095715


# Print Settings

Printer: Various
Rafts: Doesn't Matter
Supports: No
Resolution: 0,20 - 0,32
Infill: 0% - 30%

Notes: 
Walls are  always multiples of 0.6mm - so I strongly recommend printing with a extrusion width/wall width of 0.6/1.2mm to get best results.
(You don't have to use a 0.6 nozzle, 0.4 is what I'm using)

<b>Added a video regarding this to the bottom of this page.</b>

The back part has to be printed in 2 steps:

1. Print the first 1.5mm in black. 
2. Continue printing with white material for the reflectors

There's multiple ways of doing this so I suggest using a way that fits your slicer/printer combination. 

I was using Simplify3D on a Marlin-FW. So I simply added a M600 command at layer 6 (printing @0.25mm layer height).

Other slicers/printers may require different commands.




# How I Designed This

The main goal was to get something done that can be built very easily, doesn't require soldering tens or hundreds of leds and is relatively easy and fast to print.

To get this done I had to sacrifice even diffusion inside the cells - but using the reflectors and some backlight film instead of copy paper turned out quite nice. 

I tried to simplify this even more by printing some kind of diffusers right in. But that didn't work due to many reasons. One of them being bad results using white pla (too much light loss), another one the amount of required layers when using clear pla. While some of the prototypes I've built might have been easily printable on dual extrusion/mmu machines, it would have required multiple filament changes on each part. Print time was also an argument: multi-color prints take considerably longer, while changing filament on one of the parts is done very quick.

Edit/Note: As far as I can tell for an even diffusion it's more important having 2 thin layers of material than background diffusion. If using 2 layers of diffusive material I'd skip printing the white parts on the LGC_Frame_Back_v1.STL.

![Alt text](https://cdn.thingiverse.com/assets/5d/3d/d8/a8/2d/IMG_9306.jpg)
Some of the many failed experiments I went through while designing this...

# Building the Grid

## Required Parts

1: LGC_Frame_Back_v1.STL
2: LGC_Frame_v1.STL
3: LGC_Front_Bezel_Thin_v1.STL

The arduino you can see in the pictures is just there so I could test various things while building this. You don't have to connect it yet, watch the other things documentations.

![Alt text](https://cdn.thingiverse.com/assets/b6/eb/06/11/78/IMG_9221.jpg)
Only these 3 parts are required for the grid itself

## WS2812 LEDs

This is how the leds will be put inside the grid. Don't get confused by the black/white strip I'm using - I was using multiple shorter led strips I had lying around and soldered them together.

Pre-bend the strip slightly and check you don't have to bend on any solder joints.


![Alt text](https://cdn.thingiverse.com/assets/63/cb/b4/47/1d/IMG_9227.jpg)
Be careful at the 180° bends

Put the leds inside the frame as shown.

![Alt text](https://cdn.thingiverse.com/assets/46/3f/d3/33/4c/IMG_9229.jpg)

![Alt text](https://cdn.thingiverse.com/assets/96/4a/52/29/aa/IMG_9237.jpg)
Begin here and watch part orientation.

![Alt text](https://cdn.thingiverse.com/assets/68/40/4c/d3/e1/IMG_9233.jpg)
All 83 leds (77 + 3 * 2 between columns) inside the grid.

Now it's time to put the reflectors in. Align parts as shown...

![Alt text](https://cdn.thingiverse.com/assets/d6/ec/3a/36/35/IMG_9234.jpg)

![Alt text](https://cdn.thingiverse.com/assets/87/98/50/35/42/IMG_9243.jpg)

![Alt text](https://cdn.thingiverse.com/assets/f0/28/7c/ee/d1/IMG_9238.jpg)

## Front

Now add a sheet of paper or any other material you want to use as a diffuser. While regular copy paper (A4, 80g/m²) will work, I'm using something else: Forex Backlight Film.
Regular paper will block more light and the colors will be less vibrant.

![Alt text](https://cdn.thingiverse.com/assets/eb/13/87/b9/53/IMG_9244.jpg)
Paper/Film cut to 120mm x 185mm...

![Alt text](https://cdn.thingiverse.com/assets/bb/6e/27/24/e3/IMG_9245.jpg)
...and held in place using the front bezel.

## Front variations

You can play around a bit by using multiple/different diffusion materials and by using multiple front bezels. See this one for example:

![Alt text](https://cdn.thingiverse.com/assets/6a/d7/f8/d0/d8/IMG_9214.jpg)

There's a black bezel with a height of 4mm on the grid, right on top of the first layer of backlight film. The upper half shows what it looks like after putting another sheet of film on top of the first bevel and adding a thin (2mm) white bevel. Be creative here, there's quite a lot you can do with a LED grid...

## Grid done

So basically the grid is done. Either use it for your own projects or continue to build the clock...

# Electronics

## Electronics Case

The electronics case will fit an Arduino (Pro Mini or Nano) and a RTC module without problems. It's mounted to the back and if you have a look at my other clocks (Retro 7 Segment, SE edition) you'll see how this works.



![Alt text](https://cdn.thingiverse.com/assets/8f/73/63/cd/d2/IMG_9266.jpg)
Electronics Case and Feet

If you don't want to use this grid in your own project and just want the clock, please have a look at these things:

Retro 7 Segment Clock:
https://www.thingiverse.com/thing:3014572

R7SC Small Edition:
https://www.thingiverse.com/thing:3095715

You will find lots of pictures, information, schematics and a video there. This thing is built the same way (power, buttons, rtc, connecting led strip).

Connections are pretty easy:

ButtonA -> Arduino D3 + GND (blue, black)
ButtonB -> Arduino D4 + GND (purple, black)
RTC module SDA -> Arduino A4 (white)
RTC module SCL -> Arduino A5 (gray)
LED strip -> resistor (300-500ohms) -> Arduino D6

LED strip, Arduino and RTC connected to a +5V power source

Schematics:

![Alt text](https://cdn.thingiverse.com/assets/25/cb/e6/b2/eb/LGCv1_001.jpg)
2 buttons, 1 arduino nano, 1 ds3231 rtc module and the above grid. All connected to a +5v power source. (open in new tab for full resolution)

## Video Instructions

The electronics are the same as on the 7 Segment Clock SE. Wire colors in the schematics above are the same as can be seen in the video. 

<iframe src="//www.youtube.com/embed/BRm6vKyph1Q" frameborder="0" allowfullscreen></iframe>

# Arduino Sketches

1: LGC_Basics_v1
This sketch will show you how to draw numbers/pixels and nothing else. Rest is up to you.


2: LGC_Sketch_v4
This is based on the Retro 7 Segment Clock. It has the same features (24/12h format, different color palettes, different color modes, LDR support for automatic brightness corrections). So please look there for usage instructions/infos: https://www.thingiverse.com/thing:3014572

# Notes/Additions

## Printing / Part Cutouts

You might notice something while slicing the parts for printing. Let's have a closer look at the frame STL:




![Alt text](https://cdn.thingiverse.com/assets/b8/b2/15/31/62/slice_a.JPG)
Layer 1 & 2 will look like this....

![Alt text](https://cdn.thingiverse.com/assets/d2/11/aa/39/57/slice_b.JPG)
...while subsequent layers will look like this.

This is on purpose. By cutting the grid with fine 0.01mm lines it will be printed without retracts on the first layers. This reduces print time and minimizes the risk of some areas not having good outlines/not sticking to the bed... and it reduces retractions from ~170 for the first 2 layers to about ~25. :)




# Front Bezel vs. Frame

## Front Frame

I've added a "Front Frame" which can be used to hide the screws on the front. It's mounted from the backside and uses - surprise - more screws! 

To keep the possibility of playing around with different diffusion materials I couldn't come up with a clip/snap-fit solution, sorry.

Mounting the frame is quite self explainatory so I'll let some pictures do the work here.

The frame is designed to be mounted on top of the whole LGC, I didn't want people need to print parts again because of minor changes.

![Alt text](https://cdn.thingiverse.com/assets/a6/bb/d2/c4/23/IMG_3588.JPG)
Lazy Grid Clock to the left, front frame and 4 mounting clips to the right.

![Alt text](https://cdn.thingiverse.com/assets/4e/16/1f/07/26/IMG_3617.JPG)
Lower 2 screws removed to get some extra 0.25mm... xD

Depending on the diameter of the screw heads you might want to remove the lower 2 ones because they are pretty close to the grid. 

![Alt text](https://cdn.thingiverse.com/assets/33/92/3f/bc/26/IMG_3592.JPG)
I've used M3x6 and M3x14 here (left/right)

Printing is straight forward. I still recommend printing this with an extrusion width of 0.6mm. I've gone for open rectangular infill on the first layer...

(Material is "Anthrazit" PLA from DasFilament.de)

How to do this in Cura:   `https://youtu.be/RjXbr_ZxQjM`


![Alt text](https://cdn.thingiverse.com/assets/71/6d/73/fb/ba/IMG_3620.JPG)
30% rectangular infill on the first layer, solid layers starting from layer 2...

![Alt text](https://cdn.thingiverse.com/assets/c3/7e/a9/14/48/IMG_3631.JPG)
...looks like this.

![Alt text](https://cdn.thingiverse.com/assets/a8/a2/af/df/c3/IMG_3626.JPG)
2 diffusion layers (backlit inkjet film), 2 bezels and 1 front frame

# Print settings/times

<iframe src="//www.youtube.com/embed/1hZofhkjUFs" frameborder="0" allowfullscreen></iframe>