// Lazy Grid Clock - Basics v1
// https://www.thingiverse.com/thing:3241802
// 27/11/2018 - Daniel Cikic

#define FASTLED_ALLOW_INTERRUPTS 0

// uncomment following lines if using rtc (A4 = SDA, A5 = SCL)
#include <FastLED.h>

#define RESX 7                        // 7 columns
#define RESY 11                       // 11 rows
#define DIGITX 3                      // width of digits (3 pixels)
#define DIGITY 5                      // height of digits (5 pixels)
#define LED_COUNT (RESX * RESY) + 6   // total number of leds
#define LED_PIN 6                     // led data in connected to d6

CRGB leds[LED_COUNT];

byte brightness = 180;
byte saturation = 255;
byte startHue = 0;

byte digits[10][DIGITX * DIGITY] = {
  { 1, 1, 1,
    1, 0, 1,
    1, 0, 1,
    1, 0, 1,
    1, 1, 1 },        // 0
  { 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 },        // 1
  { 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1 },        // 2
  { 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1 },        // 3
  { 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1 },        // 4
  { 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1 },        // 5
  { 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1 },        // 6
  { 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 },        // 7
  { 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1 },        // 8
  { 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1 }         // 9
};


void setup() {
  Serial.begin(57600);
  Serial.println("Lazy Grid Clock - Basics v1...");
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, LED_COUNT);
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 500);  // Limit power usage to 5v/500mA
  FastLED.setBrightness(brightness);
  FastLED.clear();
  FastLED.show();
}

void loop() {
  for (byte x = 0; x < RESX; x++) {
    for (byte y = 0; y < RESY; y++) {
      setPixel(x, y, startHue + y * 2);
      FastLED.show();
      delay(75);
    }
    FastLED.clear();
    startHue+=32;
  }

  for (byte i = 0; i <= 9; i++) {
    showDigit(i, random(2, 7), random(4, 11), startHue + i * 8);
    FastLED.show();
    delay(350);
    FastLED.clear();
  }

  for (byte i = 0; i <= 99; i++) {
    showNumber(i, 2, 5, startHue + i * 4);
    FastLED.show();
    delay(75);
    FastLED.clear();
  }

  startHue += 8;
}

void showDigit(byte digit, byte x, byte y, byte color) {
  for (byte i = 0; i < 15; i++) {
    if (digits[digit][i] == 1) setPixel(x + (i - ((i / DIGITX) * DIGITX)) - 2, y - (i / DIGITX), color);
  }
}

void showNumber (byte number, byte x, byte y, byte color) {
  showDigit(number % 10, x + 4, y, color);
  showDigit(number / 10, x, y, color);
}

void setPixel(byte x, byte y, byte color) {
  if (x < RESX && y < RESY) {
    if ((x % 2) == 0) {
      leds[x + (x * RESY) + y].setHSV(color, saturation, brightness);
    } else {
      leds[((x + 1) * RESY) - y + x].setHSV(color, saturation, brightness);
    }
  }
}

