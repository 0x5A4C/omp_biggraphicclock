#ifndef IODEVICE_HPP
#define IODEVICE_HPP

namespace sys
{

class IODevice
{
  public:
    IODevice()
    {
    }

    virtual ~IODevice()
    {
    }

    virtual void write(char c) = 0;
    virtual void write(const char *str)
    {
        char c;
        while ((c = *str++))
        {
            this->write(c);
        }
    }
    virtual void flush() = 0;
    virtual bool read(char &c) = 0;

  private:
    IODevice(const IODevice &);
};

} // namespace sys

#endif // IODEVICE_HPP
