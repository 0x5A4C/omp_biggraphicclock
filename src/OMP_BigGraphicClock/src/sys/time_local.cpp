#include "time_local.hpp"
#include <stdint.h>
#include <ctime>

namespace Time
{
time_t timestamp;

uint8_t tsToHour(time_t tstamp)
{
    struct tm *timeinfo;
    // time(&tstamp);
    timeinfo = localtime(&tstamp);
    return (timeinfo->tm_hour);
}
uint8_t tsToMinute(time_t tstamp)
{
    struct tm *timeinfo;
    // time(&tstamp);
    timeinfo = localtime(&tstamp);
    return (timeinfo->tm_min);
}
uint8_t tsToSecond(time_t tstamp)
{
    struct tm *timeinfo;
    // time(&tstamp);
    timeinfo = localtime(&tstamp);
    return (timeinfo->tm_sec);
}
} // namespace Time
