#ifndef TIME_LOCAL_HPP
#define TIME_LOCAL_HPP

#include <stdint.h>
#include <ctime>

namespace Time
{
// typedef long time_t;

extern  time_t timestamp;

uint8_t tsToHour(time_t tstamp);
uint8_t tsToMinute(time_t tstamp);
uint8_t tsToSecond(time_t tstamp);
}

#endif
