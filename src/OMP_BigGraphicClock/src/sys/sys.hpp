#ifndef __SYS_HPP
#define __SYS_HPP

namespace Sys
{

extern void sysInit();
extern void sysLoop();
} // namespace Sys

#endif
