#include "app/app.hpp"
#include "bsp/bsp.hpp"
// #include "sys/sys.hpp"
#include "mod/mod.h"

#include "lib/log/log.h"

Log logger;

void setup()
{
    Bsp::bspInit();
    // Sys::sysInit();
#ifndef WIN_PLATFORM
    Mod::modInit();
#endif
    App::appInit();
    
    logger << typelog::INFO << "setup done";
    logger << typelog::INFO << "INFO " << typelog::WARN << "WARN";
}

void loop()
{
    App::appLoop();
    Bsp::bspLoop();
    // Sys::sysLoop();
    // Mod::modLoop();
}

#ifdef WIN_PLATFORM

int main(void)
{
    setup();

    while (1)
    {
        loop();
    }
}

#endif