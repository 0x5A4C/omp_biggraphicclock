#ifndef __APP_HPP
#define __APP_HPP

#include <lib/ugr/color.hpp>

namespace App
{
extern void appInit();
extern void appLoop();
} // namespace App

#endif
