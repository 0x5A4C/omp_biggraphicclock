#include "app.hpp"
// #include "sys/sys.hpp"
#include "lib/ugr/rect.hpp"
#include "lib/ugr/transformAlpha.hpp"
#include "lib/ugr/transformColor.hpp"
#include "lib/ugr/viewPort.hpp"

#include <stdint.h>

#include <sys/time_local.hpp>

namespace App
{
sGfx::ViewPort vp;

#define DIGITX 3 // width of digits (3 pixels)
#define DIGITY 5 // height of digits (5 pixels)

uint8_t startHue = 0;      // hsv color 0 = red (also used as "index" for the palette
uint8_t colorOffset = 200; // default distance between colors on the color

const uint8_t digits[10][DIGITX * DIGITY] = {
    {1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1}, // 0
    {0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1}, // 1
    {1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1}, // 2
    {1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1}, // 3
    {1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1}, // 4
    {1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1}, // 5
    {1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1}, // 6
    {1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1}, // 7
    {1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1}, // 8
    {1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1}  // 9
};

void showDigit(uint8_t digit, uint8_t x, uint8_t y, sGfx::Color color)
{
    const uint8_t *digitMask = digits[digit];

    for (uint8_t x_ = 0; x_ < DIGITX; ++x_)
    {
        for (uint8_t y_ = 0; y_ < DIGITY; ++y_)
        {
            uint8_t i_ = (y_ * DIGITX) + x_;
            if (digitMask[i_] == 1)
            {
                vp.setPixel(x_ + x, y_ + y, color);
            }
        }
    }
}

sGfx::RectFill backGr(&vp);

class DisplayTIme : public sGfx::Drawable
{
  public:
    virtual void draw()
    {
        // setPixel(3, 3, color);
        displayTime();
    }

  private:
    void showDigit(uint8_t digit, uint8_t x, uint8_t y)
    {
        const uint8_t *digitMask = digits[digit];

        for (uint8_t x_ = 0; x_ < DIGITX; ++x_)
        {
            for (uint8_t y_ = 0; y_ < DIGITY; ++y_)
            {
                if (digitMask[(y_ * DIGITX) + x_] == 1)
                {
                    setPixel(x_ + x, y_ + y, color);
                }
            }
        }
    }

    void displayTime()
    {
        showDigit(Time::tsToHour(Time::timestamp) / 10, 0, 0);
        showDigit(Time::tsToHour(Time::timestamp) % 10, 4, 0);
        showDigit(Time::tsToMinute(Time::timestamp) / 10, 0, 6);
        showDigit(Time::tsToMinute(Time::timestamp) % 10, 4, 6);
    }
};

sGfx::TansformColor<DisplayTIme> timeDisplayColor;

sGfx::Color digitStartColor = sGfx::Color::blue();
sGfx::Color digitEndColor = sGfx::Color::red();
void setdigitStartColor(sGfx::Color color)
{
    digitStartColor = color;
    timeDisplayColor.setColor(color);
}

void setdigitEndColor(sGfx::Color color)
{
    digitEndColor = color;
    timeDisplayColor.setColor(color);
}

class SecMarker : public sGfx::Drawable
{
  public:
    virtual void draw()
    {
        setPixel(0, 0, color);
        setPixel(2, 0, color);
    }

    virtual void tick()
    {
    }

  private:
    unsigned long ts_;
};

sGfx::TansformAlpha<SecMarker> secMarkerAlpha;
sGfx::Color dotMarkerColor = sGfx::Color::yellow();

void appInit()
{
    backGr.setX(0);
    backGr.setY(0);
    backGr.setW(7);
    backGr.setH(11);
    backGr.setA(0xff);
    backGr.setColor(sGfx::Color::black());
    backGr.setViewport(&vp);

    secMarkerAlpha.setX(2);
    secMarkerAlpha.setY(5);
    secMarkerAlpha.setW(3);
    secMarkerAlpha.setH(1);
    secMarkerAlpha.setColor(dotMarkerColor);
    secMarkerAlpha.setA(0xff);
    secMarkerAlpha.setViewport(&vp);
    secMarkerAlpha.setParent(&backGr);

    timeDisplayColor.setX(0);
    timeDisplayColor.setY(0);
    timeDisplayColor.setW(7);
    timeDisplayColor.setH(11);
    timeDisplayColor.setColor(digitStartColor);
    // timeDisplayColor.setA(0xFF/2);
    timeDisplayColor.setViewport(&vp);
    timeDisplayColor.setParent(&backGr);

    vp.add(&timeDisplayColor);
    vp.add(&secMarkerAlpha);
    vp.add(&backGr);
}

uint8_t i = 0;
void appLoop()
{
    if (!secMarkerAlpha.isRunning())
    {
        if (i & 0x01)
        {
            secMarkerAlpha.setA(0x00);
            secMarkerAlpha.start(0xff, 500);
        }
        else
        {
            secMarkerAlpha.setA(0xff);
            secMarkerAlpha.start(0x00, 500);
        }
    }

    if (!timeDisplayColor.isRunning())
    {
        if (i & 0x01)
        {
            timeDisplayColor.setColor(digitStartColor);
            timeDisplayColor.start(digitEndColor, 500);
        }
        else
        {
            timeDisplayColor.setColor(digitEndColor);
            timeDisplayColor.start(digitStartColor, 500);
        }
        i += 1;
    }

    vp.update();
    vp.tick();
}
} // namespace App