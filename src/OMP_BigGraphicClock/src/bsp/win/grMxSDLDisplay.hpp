#ifndef GRMXSDLDISPLAY_HPP
#define GRMXSDLDISPLAY_HPP

#include "lib/ugr/color.hpp"
#include "sys/device/display/displayGraphic.hpp"

#include "./sdlGrDisp/sdlMatrixDisplay.hpp"

class GrMxSdlDisplay : public DisplayGraphic
{
  public:
    GrMxSdlDisplay()
    {
        display.init();
    }

    virtual void setPixel(int16_t x, int16_t y, sGfx::Color color)
    {
        display.setColor(color.getValue());
        display.setPixel(x, y);
    };

    virtual uint16_t getWidth()
    {
        return 0;
    }

    virtual uint16_t getHeight()
    {
        return 0;
    }

    void update()
    {
        display.update();
    };

  protected:
  private:
    SDLMatrixDisplay<8 * 4, 8, 2> display;
};

#endif
