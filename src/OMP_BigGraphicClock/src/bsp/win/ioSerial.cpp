#include <bsp/ioSerial.hpp>

#include <iostream>
// #include <ncurses.h>

bsp::IOSerial::IOSerial()
{
    // initscr();
    // cbreak();
    // noecho();
    // scrollok(stdscr, TRUE);
    // nodelay(stdscr, TRUE);
}

bsp::IOSerial::~IOSerial()
{
    // endwin();
}

void bsp::IOSerial::write(char c)
{
    std::cout << c;
}

void bsp::IOSerial::write(const char *str)
{
    std::cout << str;
}

void bsp::IOSerial::flush()
{
    std::cout.flush();
}

bool bsp::IOSerial::read(char &c)
{

    // c = getch();
    // if (c != ERR)
    {
        return true;
    }

    // int inChar = std::cin.get();
    // if (inChar != EOF)
    // {
    //     c = static_cast<char>(inChar);
    //     return true;
    // }
    // return false;

    // if (std::cin.peek() != EOF)
    // {
    //     std::cin.get(c);
    //     return true;
    // }
    // return false;

    // if (bytesAvailable())
    // {
    //     std::cin.get(c);
    //     return true;
    // }

    return false;
}

std::size_t bsp::IOSerial::bytesAvailable() const
{
    if (std::cin.gcount())
    {
        std::cout << '+';

        return true;
    }
    return false;
    // return std::cin.rdbuf()->in_avail();
}
