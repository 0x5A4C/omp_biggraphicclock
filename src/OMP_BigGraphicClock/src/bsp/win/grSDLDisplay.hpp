#ifndef GRSDLDISPLAY_HPP
#define GRSDLDISPLAY_HPP

#include "lib/ugr/color.hpp"
#include "sys/device/display/displayGraphic.hpp"

#include "./sdlGrDisp/sdlDirectDisplay.hpp"

class GrSdlDisplay : public DisplayGraphic
{
  public:
    GrSdlDisplay() //:display(SDLDisplay(8*4, 8, 10))
    {
        display.init();
    }

    virtual void setPixel(int16_t x, int16_t y, sGfx::Color color)
    {
        display.setColor(color.getValue());
        display.setPixel(x, y);
    };

    virtual uint16_t getWidth()
    {
        return 0;
    }

    virtual uint16_t getHeight()
    {
        return 0;
    }

    void update()
    {
        display.update();
    };

  protected:
  private:
    SDLDirectDisplay<640, 480> display;
};

#endif
