#ifndef GRMXSDLDISPLAY_HPP
#define GRMXSDLDISPLAY_HPP

#include "lib/ugr/color.hpp"
#include "sys/device/display/displayGraphic.hpp"

#include "./sdlGrDisp/sdlSqMatrixDisplay.hpp"

class GrSqMxSdlDisplay : public DisplayGraphic
{
  public:
    GrSqMxSdlDisplay()
    {
        display.init();
    }

    virtual void setPixel(int16_t x, int16_t y, sGfx::Color color)
    {
        display.setColor(color.getValue());
        display.setPixel(x, y);
    };

    virtual uint16_t getWidth()
    {
        return 0;
    }

    virtual uint16_t getHeight()
    {
        return 0;
    }

    void update()
    {
        display.update();
    };

  protected:
  private:
    SDLSqMatrixDisplay<7, 11, 10> display;
};

#endif
