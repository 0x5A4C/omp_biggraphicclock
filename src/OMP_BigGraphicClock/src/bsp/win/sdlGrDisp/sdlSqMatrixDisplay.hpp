
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef SDLMATRIXDISPLAY_HPP
#define SDLMATRIXDISPLAY_HPP

#include <SDL.h>
#include <iostream>
#include <stdint.h>
#include <stdlib.h>
#include <string>

template <int16_t width, int16_t height, uint8_t radius>
class SDLSqMatrixDisplay
{
  public:
    static void init();
    static void setPixel(int16_t x, int16_t y);
    static void drawFilledCircle(int x0, int y0);

    static void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 0xff)
    {
        colorR = r;
        colorG = g;
        colorB = b;
        colorA = a;

        SDL_SetRenderDrawColor(renderer, colorR, colorG, colorB, colorA);
    }

    static void setColor(uint16_t rgb565)
    {
        colorR = ((((rgb565 >> 11) & 0x1F) * 527) + 23) >> 6;
        colorG = ((((rgb565 >> 5) & 0x3F) * 259) + 33) >> 6;
        colorB = (((rgb565 & 0x1F) * 527) + 23) >> 6;

        colorA = 0xff;

        SDL_SetRenderDrawColor(renderer, colorR, colorG, colorB, colorA);
    }

    static void setColor(uint32_t rgb888)
    {
        colorR = (rgb888 >> 16) & 0xff;
        colorG = (rgb888 >> 8) & 0xff;
        colorB = rgb888 & 0xff;

        colorA = 0xff;

        SDL_SetRenderDrawColor(renderer, colorR, colorG, colorB, colorA);
    }

    static uint16_t getHeight()
    {
        return height;
    }

    static uint16_t getWidth()
    {
        return width;
    }

    static void update()
    {
        HandleEvents();
        if (!g_Running)
        {
            // SDL_DestroyTexture(texture);
            SDL_DestroyRenderer(renderer);
            SDL_DestroyWindow(window);
            SDL_Quit();
            exit(0);
        }

        SDL_RenderPresent(renderer);
    }

  protected:
  private:
    static bool g_Running;

    static void HandleKeyEvent(const SDL_Event &event)
    {
        switch (event.key.keysym.sym)
        {
        default:
            break;
        case SDLK_ESCAPE:
            g_Running = false;
            break;
        }
    }

    static void HandleEvent(const SDL_Event &event)
    {
        switch (event.type)
        {
        default:
            break;
        case SDL_QUIT:
            g_Running = false;
            break;
        case SDL_KEYDOWN:
            HandleKeyEvent(event);
            break;
        }
    }

    static void HandleEvents()
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            HandleEvent(event);
        }
    }

    static SDL_Window *window;
    static SDL_Renderer *renderer;
    static SDL_Rect rendererViewport;

    static uint8_t colorR;
    static uint8_t colorG;
    static uint8_t colorB;
    static uint8_t colorA;
};

template <int16_t width, int16_t height, uint8_t radius>
SDL_Window *SDLSqMatrixDisplay<width, height, radius>::window;
template <int16_t width, int16_t height, uint8_t radius>
SDL_Renderer *SDLSqMatrixDisplay<width, height, radius>::renderer;
template <int16_t width, int16_t height, uint8_t radius>
SDL_Rect SDLSqMatrixDisplay<width, height, radius>::rendererViewport;
template <int16_t width, int16_t height, uint8_t radius>
uint8_t SDLSqMatrixDisplay<width, height, radius>::colorR;
template <int16_t width, int16_t height, uint8_t radius>
uint8_t SDLSqMatrixDisplay<width, height, radius>::colorG;
template <int16_t width, int16_t height, uint8_t radius>
uint8_t SDLSqMatrixDisplay<width, height, radius>::colorB;
template <int16_t width, int16_t height, uint8_t radius>
uint8_t SDLSqMatrixDisplay<width, height, radius>::colorA;
template <int16_t width, int16_t height, uint8_t radius>
bool SDLSqMatrixDisplay<width, height, radius>::g_Running;

template <int16_t width, int16_t height, uint8_t radius>
void SDLSqMatrixDisplay<width, height, radius>::init()
{
    int posX = 100;
    int posY = 200;

    g_Running = true;

    // Initialize SDL
    // ==========================================================
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        // Something failed, print error and exit.
        //std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
        //return -1;
    }

    // Create and init the window
    // ==========================================================
    uint16_t X = (((width * (radius * 3)) + 1) + radius);
    uint16_t Y = (((height * (radius * 3)) + 1) + radius);

    window = SDL_CreateWindow("Display", posX, posY, X, Y, 0);

    if (window == nullptr)
    {
        //std::cout << "Failed to create window : " << SDL_GetError();
        //return -1;
    }

    // Create and init the renderer
    // ==========================================================
    // renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    //in other case dosent render anything
    renderer = SDL_CreateRenderer(window, -1, 1);

    if (renderer == nullptr)
    {
        //std::cout << "Failed to create renderer : " << SDL_GetError();
        //return -1;
    }

    // Render something
    // ==========================================================

    // Set size of renderer to the same as window
    SDL_RenderSetLogicalSize(renderer, X, Y);

    // Set color of renderer to black
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);

    // Clear the window and make it all red
    SDL_RenderClear(renderer);

    // Render the changes above ( which up until now had just happened behind the scenes )
    SDL_RenderPresent(renderer);

    // Pause program so that the window doesn't disappear at once.
    // This willpause for 4000 milliseconds which is the same as 4 seconds
    // SDL_Delay(4000);

    colorR = 0xff;
    colorG = 0xff;
    colorB = 0xff;
    colorA = 0xff;
}

template <int16_t width, int16_t height, uint8_t radius>
void SDLSqMatrixDisplay<width, height, radius>::setPixel(int16_t x, int16_t y)
{
    // SDL_RenderDrawPoint(renderer, x, y); //Renders on middle of screen.
    // drawFilledCircle(((x * (radius * 3)) + (radius * 2)), ((y * radius * 3) + (radius * 2)));

    //Get window surface
    SDL_Surface *screenSurface = SDL_GetWindowSurface(window);
    // create a black square
    SDL_Rect rect = {((x * (radius * 3)) + (2)), ((y * radius * 3) + (2)), radius * 2, radius * 2}; // x, y, width, height
    SDL_FillRect(screenSurface, &rect, SDL_MapRGB(screenSurface->format, colorR, colorG, colorB));
}

#endif /* SDLDIRECTDISPLAY_HPP */
