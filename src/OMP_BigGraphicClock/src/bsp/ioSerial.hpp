#ifndef IO_SERIAL_HPP
#define IO_SERIAL_HPP

#include "sys/device/ioDevice.hpp"
#include <cstdio>

namespace bsp
{

class IOSerial : sys::IODevice
{
  public:
    IOSerial();
    virtual ~IOSerial();

    virtual void write(char c);
    virtual void write(const char *str);
    virtual void flush();
    virtual bool read(char &c);
    std::size_t bytesAvailable() const;
};

} // namespace bsp

#endif
