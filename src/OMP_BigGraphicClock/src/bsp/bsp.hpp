#ifndef __BSP_HPP
#define   BSP_HPP

namespace Bsp
{
extern void bspInit();
extern void bspLoop();
} // namespace Bsp

#endif
