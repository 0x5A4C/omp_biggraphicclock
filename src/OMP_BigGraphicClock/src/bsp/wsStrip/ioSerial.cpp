#include <bsp/ioSerial.hpp>

#include <Arduino.h>

bsp::IOSerial::IOSerial()
{
}

bsp::IOSerial::~IOSerial()
{
}

void bsp::IOSerial::write(char c)
{
    Serial.write(c);
}

void bsp::IOSerial::write(const char *str)
{
    char c;
    while ((c = *str++))
    {
        this->write(c);
    }
}

void bsp::IOSerial::flush()
{
    Serial.flush();
}

bool bsp::IOSerial::read(char &c)
{
    return Serial.readBytes(&c, 1);
}

std::size_t bsp::IOSerial::bytesAvailable() const
{
    return Serial.available();
}
