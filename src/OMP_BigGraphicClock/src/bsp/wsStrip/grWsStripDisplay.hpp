#ifndef GRWSSTRIPDISPLAY_HPP
#define GRWSSTRIPDISPLAY_HPP

#include "lib/ugr/color.hpp"
#include "sys/device/display/displayGraphic.hpp"

#include <FastLED.h>

#include <stdint.h>

#define RESX 7                      // 7 columns
#define RESY 11                     // 11 rows
#define DIGITX 3                    // width of digits (3 pixels)
#define DIGITY 5                    // height of digits (5 pixels)
#define LED_COUNT (RESX * RESY) + 6 // total number of leds
#define LED_PIN D4                  // led data in connected to d6

CRGB ledStrip[LED_COUNT];

class GrMxSdlDisplay : public DisplayGraphic
{
  public:
    GrMxSdlDisplay() : draw(false)
    {
        FastLED.addLeds<WS2812B, LED_PIN, GRB>(ledStrip, LED_COUNT);
        FastLED.setMaxPowerInVoltsAndMilliamps(5, 500); // Limit power usage to 5V/500ma
        FastLED.setDither(0);
        FastLED.setBrightness(0x0ff);
        FastLED.clear();
        FastLED.show();
    }

    virtual void setPixel(int16_t x, int16_t y, sGfx::Color color)
    {
        uint8_t pixel = 0;
        if (x < RESX && y < RESY)
        {
            y = RESY - y - 1;
            if ((x % 2) == 0)
                pixel = x + (x * RESY) + y;
            else
                pixel = ((x + 1) * RESY) - y + x;

            ledStrip[pixel] = color.getValue();
        }

        draw = true;
    };

    virtual uint16_t getWidth()
    {
        return RESX;
    }

    virtual uint16_t getHeight()
    {
        return RESY;
    }

    void update()
    {
        if (!draw)
        {
            return;
        }

        draw = false;
        FastLED.setBrightness(0xff);
        FastLED.show();
    };

  protected:
  private:
    bool draw;
};

#endif
