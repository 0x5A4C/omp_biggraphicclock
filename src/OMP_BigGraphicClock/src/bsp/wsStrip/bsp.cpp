#include "bsp/bsp.hpp"

#include <Arduino.h>
#include <ESP8266WiFi.h>

#include <sys/time_local.hpp>

#include "grWsStripDisplay.hpp"

#include <Time.h>

#include "bsp/ioSerial.hpp"

typedef GrMxSdlDisplay display_t;
display_t bspDisplay;
DisplayGraphic *display = &bspDisplay;

bsp::IOSerial ioSerial;

namespace Bsp
{
void bspInit()
{
    Serial.begin(115200);
    Serial.printf("\n\n -------------------------------------------------------------------------------- \n");

    
    bspDisplay.setPixel(0, 0, sGfx::Color::red());
    bspDisplay.setPixel(1, 1, sGfx::Color::white());
    bspDisplay.setPixel(2, 2, sGfx::Color::blue());
}

void bspLoop()
{
    Time::timestamp = (hour() * 60 * 60) + (minute() * 60) + (second());

    bspDisplay.update();
}

} // namespace Bsp
