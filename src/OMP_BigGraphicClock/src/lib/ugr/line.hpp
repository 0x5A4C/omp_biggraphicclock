
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef LINE_HPP
#define LINE_HPP

#include "drawable.hpp"
#include <stdint.h>

namespace sGfx
{
class Line : public Drawable
{
  public:
    Line();
    Line(ViewPort *viewport);
    Line(int16_t x0, int16_t y0, int16_t x1, int16_t y1);
    Line(int16_t x0, int16_t y0, int16_t x1, int16_t y1, ViewPort *viewport);
    void draw();
    void set(int16_t x0, int16_t y0, int16_t x1, int16_t y1);
    virtual void setPixel(int16_t x, int16_t y, Color color);

  protected:
    int16_t x0;
    int16_t y0;
    int16_t x1;
    int16_t y1;

  private:
};

class LineV : public Line
{
  public:
    LineV();
    LineV(ViewPort *viewport);
    LineV(int16_t x0, int16_t y0, int16_t h);
    LineV(int16_t x0, int16_t y0, int16_t h, ViewPort *viewport);
    void set(int16_t x0, int16_t y0, int16_t h);

  protected:
  private:
};

class LineH : public Line
{
  public:
    LineH();
    LineH(ViewPort *viewport);
    LineH(int16_t x0, int16_t y0, int16_t w);
    LineH(int16_t x0, int16_t y0, int16_t w, ViewPort *viewport);
    void set(int16_t x0, int16_t y0, int16_t w);

  protected:
  private:
};
} // namespace sGfx

#endif /* LINE_HPP */
