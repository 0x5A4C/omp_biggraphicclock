/*! Short Description on the first line

    Detailed description...
    ...
 */

#include "bmpXbmAnimated.hpp"

sGfx::BmpXbmAnimated::BmpXbmAnimated() : BmpXbm(), duration(0), pendingDuration(0), animated(false), slideNum(0), numOfSlides(0)
{
}

sGfx::BmpXbmAnimated::BmpXbmAnimated(ViewPort *viewport) : BmpXbm(viewport), duration(0), pendingDuration(0), animated(false), slideNum(0), numOfSlides(0)
{
}

void sGfx::BmpXbmAnimated::draw()
{
    BmpXbm::draw();
}

void sGfx::BmpXbmAnimated::setDuration(uint16_t duration)
{
    this->duration = duration;
}

void sGfx::BmpXbmAnimated::setNumOfSlides(uint16_t numOfSlides)
{
    this->numOfSlides = numOfSlides;
}

void sGfx::BmpXbmAnimated::setBitmap(const uint8_t *bmpToDraw)
{
    bitmap = bmpToDraw;
    bitmapStartAddr = bmpToDraw;
}

void sGfx::BmpXbmAnimated::tick()
{
    if (animated)
    {
        pendingDuration++;
        if (pendingDuration >= duration)
        {
            pendingDuration = 0;
            if (slideNum > numOfSlides - 1)
            {
                slideNum = 0;
            }
            bitmap = bitmapStartAddr + (slideNum * (w / 8) * h);

            slideNum += 1;
        }
    }
}

void sGfx::BmpXbmAnimated::start()
{
    animated = true;
    slideNum = 0;
}

void sGfx::BmpXbmAnimated::stop()
{
    animated = false;
}
