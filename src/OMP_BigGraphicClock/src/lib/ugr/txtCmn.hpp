
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef TXTCMN_HPP
#define TXTCMN_HPP

#include <stdint.h>

#include "control.hpp"
#include "drawable.hpp"

namespace sGfx
{

class TxtCmn : public Control
{
  public:
    TxtCmn() : Control(){};
    TxtCmn(ViewPort *viewport) : Control(viewport){};
    virtual void drawChar(unsigned char c) = 0;
    void setChar(unsigned char c)
    {
        charToDraw = c;
    }

    void draw()
    {
        drawChar(charToDraw);
    }

    void setViewport(ViewPort *viewport)
    {
        this->viewport = viewport;
    }

    virtual uint8_t getWidth() = 0;
    virtual uint8_t getHeight() = 0;

  protected:
  private:
    char charToDraw;
};
} // namespace sGfx
#endif /* TXTCMN_HPP */
