
#ifndef TRANSFORMXY_HPP
#define TRANSFORMXY_HPP

#include "transform.hpp"

namespace sGfx
{

template <class T>
class TansformXY : public Tansform<T>
{
  public:
    void start(int16_t endX, int16_t endY, uint16_t duration, Easing easingX = &Easings::linearTween, Easing easingY = &Easings::linearTween)
    {
        this->counter = 0;
        this->startX = T::x;
        this->startY = T::y;
        this->endX = endX;
        this->endY = endY;
        this->duration = duration;
        this->easingX = easingX;
        this->easingY = easingY;

        this->isOn = true;
    }

    virtual void tick()
    {
        T::tick();

        if (isOn)
        {
            if (counter <= (uint32_t)(duration))
            {
                int16_t deltaX = easingX(counter, 0, endX - startX, duration);
                int16_t deltaY = easingY(counter, 0, endY - startY, duration);
                T::setX(startX + deltaX);
                T::setY(startY + deltaY);

                counter++;
            }
            else
            {
                isOn = false;
                counter = 0;
#if 1
                if (endCallback)
                {
                    endCallback();
                }
#endif
            }
        }
    }

  protected:
    uint16_t duration;
    int16_t startX;
    int16_t startY;
    int16_t endX;
    int16_t endY;
    Easing easingX;
    Easing easingY;
#if 1
    std::function<void(void)> endCallback;
#endif
};


} // namespace sGfx

#endif /* TRANSFORXY_HPP */
