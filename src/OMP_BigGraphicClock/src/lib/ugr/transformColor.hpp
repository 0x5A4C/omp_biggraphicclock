#ifndef TRANSFORMCOLOR_HPP
#define TRANSFORMCOLOR_HPP

#include "math.h"
#include "transform.hpp"

namespace sGfx
{

template <class T>
class TansformColor : public Tansform<T>
{
  public:
    void start(sGfx::Color endColor, uint16_t duration, Easing easing = &Easings::linearTween)
    {
        this->counter = 0;
        this->startColor = this->color;
        this->endColor = endColor;
        this->duration = duration;
        this->easing = easing;

        this->isOn = true;
    }

    virtual void tick()
    {
        T::tick();

        if (this->isOn)
        {
            if (this->counter <= this->duration)
            {
                auto red = static_cast<uint8_t>((this->easing(this->counter, 0, this->endColor.getRed() - this->startColor.getRed(), this->duration)));
                auto green = static_cast<uint8_t>((this->easing(this->counter, 0, this->endColor.getGreen() - this->startColor.getGreen(), this->duration)));
                auto blue = static_cast<uint8_t>((this->easing(this->counter, 0, this->endColor.getBlue() - this->startColor.getBlue(), this->duration)));
                this->color.setRed(this->startColor.getRed() + red);
                this->color.setGreen(this->startColor.getGreen() + green);
                this->color.setBlue(this->startColor.getBlue() + blue);
                ++this->counter;
            }
            else
            {
                if (!(this->color == this->endColor))
                {
                    this->color = this->endColor;
                }

                this->isOn = false;
                this->counter = 0;
#if 1
                if (this->endCallback)
                {
                    this->endCallback();
                }
#endif
            }
        }
    }

  protected:
    sGfx::Color startColor;
    sGfx::Color endColor;
};

} // namespace sGfx

#endif
