
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef TXT4x8_HPP
#define TXT4x8_HPP

#include <stdint.h>

#include "txtCmn.hpp"

namespace sGfx
{

const unsigned char font4x8[][4] =
    {
        {0x7e, 0x81, 0x81, 0x7e}, // 0
        {0x02, 0xff, 0x00, 0x00}, // 1
        {0xf1, 0x89, 0x89, 0x86}, // 2
        {0x81, 0x81, 0x89, 0x76}, // 3
        {0x07, 0x08, 0x08, 0xff}, // 4
        {0x87, 0x89, 0x89, 0x71}, // 5
        {0x7f, 0x88, 0x88, 0x70}, // 6
        {0x01, 0xe1, 0x19, 0x07}, // 7
        {0x76, 0x89, 0x89, 0x76}, // 8
        {0x0e, 0x11, 0x11, 0xfe}, // 9
        {0x44, 0x00, 0x00, 0x00}, // :
        {0x00, 0x00, 0x00, 0x00}  //
};

class Txt4x8 : public TxtCmn
{
  public:
    Txt4x8() : TxtCmn(){};
    Txt4x8(ViewPort *viewport) : TxtCmn(viewport){};

    virtual void drawChar(unsigned char c)
    {
        uint8_t charToDraw;

        if ((c < '0') || (c > '9'))
        {
            charToDraw = 11; //space
        }
        else
        {
            charToDraw = c - '0';
        }

        if (viewport != NULL)
        {
            for (int8_t i = 0; i < 4; i++)
            {
                uint8_t line;

                line = font4x8[charToDraw][i];
                for (uint8_t j = 0; j < 8; j++)
                {
                    if ((line >> j) & 0x01)
                    {
                        setPixel(i, j, this->color);
                    }
                }
            }
        }
    }

    virtual uint8_t getWidth()
    {
        return 4;
    }

    virtual uint8_t getHeight()
    {
        return 8;
    }

  protected:
  private:
};
} // namespace sGfx

#endif /* TXT5x7_HPP */
