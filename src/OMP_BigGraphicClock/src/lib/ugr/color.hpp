#ifndef COLOR_HPP
#define COLOR_HPP

#include "lib/cmn/String.h"
#include <cstring>
#include <map>
#include <stdint.h>

#define ALWAYS_INLINE inline

namespace sGfx
{
class Color
{
  public:
    static ALWAYS_INLINE Color white()
    {
        // return Color(0xffffff);
        return getColorValueByName("white");
    };
    static ALWAYS_INLINE Color yellow()
    {
        // return Color(0xFFFF00);
        return getColorValueByName("yellow");
    };
    static ALWAYS_INLINE Color magenta()
    {
        // return Color(0xF81F);
        return getColorValueByName("magenta");
    };
    static ALWAYS_INLINE Color red()
    {
        // return Color(0xff0000);
        return getColorValueByName("red");
    };
    static ALWAYS_INLINE Color orange()
    {
        // return Color(0xFD20);
        return getColorValueByName("orange");
    };
    static ALWAYS_INLINE Color sliver()
    {
        // return Color(0xC618);
        return getColorValueByName("sliver");
    };
    static ALWAYS_INLINE Color gray()
    {
        // return Color(0x8410);
        return getColorValueByName("gray");
    };
    static ALWAYS_INLINE Color maroon()
    {
        // return Color(0x8000);
        return getColorValueByName("maroon");
    };
    static ALWAYS_INLINE Color lime()
    {
        // return Color(0x07E0);
        return getColorValueByName("lime");
    };
    static ALWAYS_INLINE Color green()
    {
        // return Color(0x00ff00);
        return getColorValueByName("green");
    };
    static ALWAYS_INLINE Color blue()
    {
        // return Color(0x0000ff);
        return getColorValueByName("blue");
    };
    static ALWAYS_INLINE Color navy()
    {
        // return Color(0x0010);
        return getColorValueByName("navy");
    };
    static ALWAYS_INLINE Color black()
    {
        // return Color(0x000000);
        return getColorValueByName("black");
    };

    /**
    * @param	red
    * 		Range [0..255]
    * @param	green
    * 		Range [0..255]
    * @param	blue
    * 		Range [0..255]
    */
    //  Color(uint8_t red, uint8_t green, uint8_t blue) : color(((static_cast<uint16_t>(red >> 3) << 11) |
    //                                                           (static_cast<uint16_t>(green >> 2) << 5) |
    //                                                           static_cast<uint16_t>(blue >> 3)))

    Color(uint8_t red, uint8_t green, uint8_t blue) : color(((static_cast<uint32_t>(red) << 16) |
                                                             (static_cast<uint32_t>(green) << 8) |
                                                             static_cast<uint32_t>(blue)))

    {
    }

    Color(uint32_t color) : color(color)
    {
    }

    Color() : color(0xffffff)
    {
    }

    static Color getColorByName(const char *name)
    {
        // static std::map<const char *, Color> s_mapStringValues = {{"white", white()},
        //                                                           {"yellow", yellow()},
        //                                                           {"magenta", magenta()},
        //                                                           {"red", red()},
        //                                                           {"orange", orange()},
        //                                                           {"sliver", sliver()},
        //                                                           {"gray", gray()},
        //                                                           {"maroon", maroon()},
        //                                                           {"lime", lime()},
        //                                                           {"green", green()},
        //                                                           {"blue", blue()},
        //                                                           {"navy", navy()},
        //                                                           {"black", black()}};

        // return (s_mapStringValues[name]);

        return getColorValueByName(name);
    }

    struct cmp_str
    {
        bool operator()(char const *a, char const *b) const
        {
            // return std::strcmp(a, b) < 0;
            return 0;
        }
    };

    static Color getColorValueByName(std::string name)
    {

        static std::map<std::string, uint32_t> nameToValue = {{"white", 0xffffff},
                                                                        {"yellow", 0xFFFF00},
                                                                        {"magenta", 0xF81F},
                                                                        {"red", 0xff0000},
                                                                        {"orange", 0xFD20},
                                                                        {"sliver", 0xC618},
                                                                        {"gray", 0x8410},
                                                                        {"maroon", 0x8000},
                                                                        {"lime", 0x07E0},
                                                                        {"green", 0x00ff00},
                                                                        {"blue", 0x0000ff},
                                                                        {"navy", 0x0010},
                                                                        {"black", 0x000000}};

        // Serial.print(">>-->");

        // std::map<const char *, uint32_t>::const_iterator pos = nameToValue.find("name");
        // if (pos == nameToValue.end())
        // {
        //     Serial.print("error iterator");
        // }
        // else
        // {
        //     uint32_t value = pos->second;
        //     Serial.print(value);
        // }

        // Serial.print("\n");
        // Serial.print(nameToValue[name]);

        return Color(nameToValue[name]);
    }

    static Color getColorByName(const String name)
    {
        return (getColorByName(name.c_str()));
    }

    inline uint32_t getValue() const
    {
        return color;
    }

    inline uint8_t getRed() const
    {
        return (color & 0x00ff0000) >> 16;
    }

    inline uint8_t getGreen() const
    {
        return (color & 0x0000ff00) >> 8;
    }

    inline uint8_t getBlue() const
    {
        return (color & 0x000000ff);
    }

    inline void setRed(uint8_t red)
    {
        color &= 0xff00ffff;
        color |= static_cast<uint32_t>(red) << 16;
    }

    inline void setGreen(uint8_t green)
    {
        color &= 0xffff00ff;
        color |= static_cast<uint32_t>(green) << 8;
    }

    inline void setBlue(uint8_t blue)
    {
        color &= 0xffffff00;
        color |= static_cast<uint32_t>(blue);
    }

    bool operator==(const Color &other) const
    {
        return (color == other.color);
    }

    //  uint16_t rgb888torgb565(uint8_t *rgb888Pixel)
    //  {
    //      uint8_t red = rgb888Pixel[0];
    //      uint8_t green = rgb888Pixel[1];
    //      uint8_t blue = rgb888Pixel[2];

    //      uint16_t b = (blue >> 3) & 0x1f;
    //      uint16_t g = ((green >> 2) & 0x3f) << 5;
    //      uint16_t r = ((red >> 3) & 0x1f) << 11;

    //      return (uint16_t)(r | g | b);
    //  }

  private:
    uint32_t color;

    // static std::map<const char *, Color> s_mapStringValues = {{"white", Color::white()},
    //                                                    {"yellow", yellow()},
    //                                                    {"magenta", magenta()},
    //                                                    {"red", red()},
    //                                                    {"orange", orange()},
    //                                                    {"sliver", sliver()},
    //                                                    {"gray", gray()},
    //                                                    {"maroon", maroon()},
    //                                                    {"lime", lime()},
    //                                                    {"green", green()},
    //                                                    {"blue", blue()},
    //                                                    {"navy", navy()},
    //                                                    {"black", black()}};
};

} // namespace sGfx

#endif
