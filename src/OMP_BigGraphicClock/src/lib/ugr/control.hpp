#ifndef CONTROL_HPP
#define CONTROL_HPP

#include "control.hpp"
#include "drawable.hpp"

namespace sGfx
{
class Control : public Drawable
{
  public:
    Control() : Drawable()
    {
    }

    Control(ViewPort *viewport) : Drawable(viewport)
    {
    }

  protected:
  private:
};
} // namespace sGfx

#endif
