/*! Short Description on the first line

    Detailed description...
    ...
 */

#include "bmpXbm.hpp"

sGfx::BmpXbm::BmpXbm() : Drawable(), bitmap(NULL)
{
}

sGfx::BmpXbm::BmpXbm(ViewPort *viewport) : Drawable(viewport), bitmap(NULL)
{
    this->viewport = viewport;
}

void sGfx::BmpXbm::draw()
{
    int16_t pixX = this->x;
    int16_t pixY = this->y;
    int16_t byteWidth = (w + 7) / 8;
    uint8_t byte = 0;

    if (bitmap == NULL)
    {
        return;
    }

    for (int16_t j = 0; j < h; j++, pixY++)
    {
        for (int16_t i = 0; i < w; i++)
        {
            if (i & 7)
            {
                byte >>= 1;
            }
            else
            {
                byte = bitmap[j * byteWidth + i / 8];
            }

            if (byte & 0x01)
            {
                setPixel(pixX + i, pixY, color);
            }
        }
    }
}

void sGfx::BmpXbm::setBitmap(const uint8_t *bmpToDraw)
{
    bitmap = bmpToDraw;
}
#if 0
void sGfx::BmpXbm::set(int16_t x, int16_t y, int16_t w, int16_t h)
{
   this->x = x;
   this->y = y;
   this->w = w;
   this->h = h;
}

void sGfx::BmpXbm::setX(int16_t x)
{
   this->x = x;
}

void sGfx::BmpXbm::setY(int16_t y)
{
   this->y = y;
}

void sGfx::BmpXbm::setW(int16_t w)
{
   this->w = w;
}

void sGfx::BmpXbm::setH(int16_t h)
{
   this->h = h;
}

void sGfx::BmpXbm::setViewport(ViewPort* viewport)
{
   this->viewport = viewport;
}
#endif
