#ifndef TRANSFORMALPHA_HPP
#define TRANSFORMALPHA_HPP

#include "math.h"
#include "transform.hpp"

namespace sGfx
{

template <class T>
class TansformAlpha : public Tansform<T>
{
  public:
    void start(uint8_t endAlpha, uint16_t duration, Easing easing = &Easings::linearTween)
    {
        this->counter = 0;
        this->startAlpha = this->alpha;
        this->endAlpha = endAlpha;
        this->duration = duration;
        this->easing = easing;

        this->isOn = true;
    }

    virtual void tick()
    {
        T::tick();

        if (this->isOn)
        {
            if (this->counter <= this->duration)
            {
                auto deltaAlpha = static_cast<uint8_t>((this->easing(this->counter, 0, this->endAlpha - this->startAlpha, this->duration)));
                this->setA(this->startAlpha + deltaAlpha);

                ++this->counter;
            }
            else
            {
                this->isOn = false;
                this->counter = 0;
#if 1
                if (this->endCallback)
                {
                    this->endCallback();
                }
#endif
            }
        }
    }

  protected:
    uint8_t startAlpha;
    uint8_t endAlpha;
};

} // namespace sGfx

#endif
