
#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP

#if 1
#include <functional>
#endif
#include "easing/easing.hpp"
#include "viewPort.hpp"

namespace sGfx
{
template <class T>
class Tansform : public T
{
  public:
    Tansform() : T(),
                 isOn(false),
                 counter(0)
    {
    }

    Tansform(ViewPort *viewport) : T(viewport),
                                   isOn(false),
                                   counter(0)
    {
    }

    virtual ~Tansform()
    {
    }

    virtual bool isRunning() const
    {
        return isOn;
    }

    void cancel()
    {
        isOn = false;
    }

    virtual void tick() = 0;

    //idea from:
    // - https://stackoverflow.com/questions/14189440/c-class-member-callback-simple-examples
    //other links:
    // - https://stackoverflow.com/questions/2298242/callback-functions-in-c
    // - http://tedfelix.com/software/c++-callbacks.html
    // - https://stackoverflow.com/questions/21806632/how-to-properly-check-if-stdfunction-is-empty-in-c11
#if 1
    void addEndHandler(std::function<void(void)> callback)
    {
        endCallback = callback;
    }
#endif
  protected:
    bool isOn;
    uint16_t counter;
    uint16_t duration;
    Easing easing;
#if 1
    std::function<void(void)> endCallback;
#endif
};

} // namespace sGfx

#endif /* TRANSFORM_HPP */
