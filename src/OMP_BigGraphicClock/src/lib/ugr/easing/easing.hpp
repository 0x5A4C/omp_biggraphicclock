
#ifndef EASING_HPP
#define EASING_HPP

#include <stdint.h>

class Easings
{
public:
   static float linearTween (float t, float b, float c, float d);
   static float easeInQuad (float t, float b, float c, float d);
   static float easeOutQuad (float t, float b, float c, float d);
   static float easeInOutQuad (float t, float b, float c, float d);
   static float easeInCubic (float t, float b, float c, float d);
   static float easeOutCubic (float t, float b, float c, float d);
   static float easeInOutCubic (float t, float b, float c, float d);
   static float easeInQuart (float t, float b, float c, float d);
   static float easeOutQuart (float t, float b, float c, float d);
   static float easeInOutQuart (float t, float b, float c, float d);
   static float easeInQuint (float t, float b, float c, float d);
   static float easeOutQuint (float t, float b, float c, float d);
   static float easeInOutQuint (float t, float b, float c, float d);
   static float easeInSine (float t, float b, float c, float d);
   static float easeOutSine (float t, float b, float c, float d);
   static float easeInOutSine (float t, float b, float c, float d);
   static float easeInExpo (float t, float b, float c, float d);
   static float easeOutExpo (float t, float b, float c, float d);
   static float easeInOutExpo (float t, float b, float c, float d);
   static float easeInCirc (float t, float b, float c, float d);
   static float easeOutCirc (float t, float b, float c, float d);
   static float easeInOutCirc (float t, float b, float c, float d);
   static float easeInElastic (float t, float b, float c, float d, float a=0, float p=0);
   static float easeOutElastic (float t, float b, float c, float d, float a=0, float p=0);
   static float easeInOutElastic (float t, float b, float c, float d, float a=0, float p=0);
   static float easeInElastic (float t, float b, float c, float d);
   static float easeOutElastic (float t, float b, float c, float d);
   static float easeInOutElastic (float t, float b, float c, float d);
   static float easeInBack (float t, float b, float c, float d, float s=1.70158);
   static float easeOutBack (float t, float b, float c, float d, float s=1.70158);
   static float easeInOutBack (float t, float b, float c, float d, float s=1.70158);
   static float easeInBack (float t, float b, float c, float d);
   static float easeOutBack (float t, float b, float c, float d);
   static float easeInOutBack (float t, float b, float c, float d);
   static float easeInBounce (float t, float b, float c, float d);
   static float easeOutBounce (float t, float b, float c, float d);
   static float easeInOutBounce (float t, float b, float c, float d);
};

typedef float (* Easing)(float , float , float , float );

#endif
