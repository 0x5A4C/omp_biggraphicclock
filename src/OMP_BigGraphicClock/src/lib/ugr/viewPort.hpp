
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef VIEWPORT_HPP
#define VIEWPORT_HPP

#include <stdint.h>

#include "bsp/bsp.hpp"
#include "color.hpp"

//>>--->platform
// #include "bsp/esp/led8x32Display.hpp"
// typedef Led8x32Display display_t;
//>>--->platform
// #include "bsp/esp/dummyDisplay.hpp"
// typedef DummyDisplay display_t;
// >>--->platform
// #include "bsp/win/mxLedGrSDLDisplay.hpp"
// typedef MxGrSdlDisplay display_t;
// >>--->platform
// #include "bsp/win/grSDLDisplay.hpp"
// typedef GrSdlDisplay display_t;
//>>--->platform
// #include "bsp/win/grMxSDLDisplay.hpp"
// typedef GrMxSdlDisplay display_t;

#include <sys/device/display/displayGraphic.hpp>

extern DisplayGraphic *display;

namespace sGfx
{
class Drawable;

class ViewPort
{
  public:
    ViewPort();
    ViewPort(int16_t x, int16_t y);
    ViewPort(ViewPort *parent);
    ViewPort(int16_t x, int16_t y, ViewPort *parent);

    void gotoXY(int16_t x, int16_t y);
    uint16_t getCurX();
    uint16_t getCurY();
    virtual void setPixel(int16_t x, int16_t y, Color color);
    virtual void update(void);
    uint16_t getHeight();
    uint16_t getWidth();
    void add(Drawable *child);
    virtual void tick();
    void setDisplay(DisplayGraphic *display);

  private:
    int16_t x, y;
    int16_t xCur, yCur;

    ViewPort *parent;
    Drawable *child;
    // DisplayGraphic* display;
};

} // namespace sGfx

#endif /* ViewPort_HPP */
