#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <iostream>

enum class typelog
{
    DEBUG,
    INFO,
    WARN,
    ERROR
};

class Log
{
public:
    Log(typelog level = typelog::DEBUG, bool headers = true) : headers_(headers), level_(level)
    {
    }

    ~Log()
    {
        if (opened_)
        {
            std::cout << std::endl;
        }
        opened_ = false;
    }

    template <class T>
    Log &operator<<(const T &msg)
    {
        if (msgLevel_ >= level_)
        {
            std::cout << msg;
            opened_ = true;
        }
        return *this;
    }

    Log &operator<<(const typelog &level)
    {
        msgLevel_ = level;

        if (opened_)
        {
            std::cout << std::endl;
        }

        if (headers_)
        {
            std::cout << ("[" + getLabel(msgLevel_) + "]");
        }

        return *this;
    }

private:
    bool headers_ = true;
    typelog level_ = typelog::DEBUG;
    typelog msgLevel_ = typelog::DEBUG;
    bool opened_ = false;

    inline std::string getLabel(typelog type)
    {
        std::string label;
        switch (type)
        {
        case typelog::DEBUG:
            label = "DEBUG";
            break;
        case typelog::INFO:
            label = "INFO ";
            break;
        case typelog::WARN:
            label = "WARN ";
            break;
        case typelog::ERROR:
            label = "ERROR";
            break;
        }
        return label;
    }
};

#endif
